const { React } = window;

class FormMeme extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="comments">
        <form>
          <div className="row">
            <div className="col-25">
              <label>Awesome Meme</label>
            </div>
            <div className="col-75">
              <input
                type="file"
                id="lname"
                name="lastname"
                placeholder="Your last name.."
              />
            </div>
          </div>
          <div className="row">
            <div className="col-25">
              <label>Description</label>
            </div>
            <div className="col-75">
              <textarea
                id="subject"
                name="subject"
                placeholder="Write something an awesome meme Description.."
              />
            </div>
          </div>
          <center>
            <button id="memeButton">Upload</button>
          </center>
        </form>
      </div>
    );
  }
}
