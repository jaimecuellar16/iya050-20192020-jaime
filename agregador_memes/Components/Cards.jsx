const { React } = window;

class Cards extends React.Component {
  constructor(props) {
    super(props);
  }
  changeColor1() {
    document.getElementById("shareButton1").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton1").innerHTML = "SHARED";
  }

  changeColor2() {
    document.getElementById("shareButton2").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton2").innerHTML = "SHARED";
  }

  changeColor3() {
    document.getElementById("shareButton3").style.backgroundColor = "purple"; // backcolor
    document.getElementById("shareButton3").innerHTML = "SHARED";
  }
  render() {
    return (
      <div>
        <div className="card">
          <img
            src="https://www.quirkybyte.com/wp-content/uploads/2018/03/Captain-America-1.jpg"
            alt="Avatar"
          />
          <div className="container">
            <p id="p1">The misterious thing called life</p>
            <h4>
              <button id="memeButton">+UP</button>
              <button id="memeButton">-DOWN</button>
              <button id="memeButton">COMMENT</button>
              <button id="shareButton1" onClick={this.changeColor1}>
                SHARE
              </button>
            </h4>
          </div>
        </div>

        <div className="card">
          <img
            src="https://laverdadnoticias.com/__export/1581693762179/sites/laverdad/img/2020/02/14/viral_los_mejores_memes_del_dxa_de_san_valentin_x14_de_febrerox.jpg_1902800913.jpg"
            alt="Avatar"
          />
          <div className="container">
            <p id="p2">F**K LOVE...</p>
            <h4>
              <button id="memeButton">+UP</button>
              <button id="memeButton">-DOWN</button>
              <button id="memeButton">COMMENT</button>
              <button id="shareButton2" onClick={this.changeColor2}>
                SHARE
              </button>
            </h4>
          </div>
        </div>

        <div className="card">
          <img
            src="https://m.eldiario.es/fotos/memes-calima-Canarias_EDIIMA20200224_0333_19.jpg"
            alt="Avatar"
          />
          <div className="container">
            <p id="p3">The SHIT GET SERIOUS</p>
            <h4>
              <button id="memeButton">+UP</button>
              <button id="memeButton">-DOWN</button>
              <button id="memeButton">COMMENT</button>
              <button id="shareButton3" onClick={this.changeColor3}>
                SHARE
              </button>
            </h4>
          </div>
        </div>
      </div>
    );
  }
}
