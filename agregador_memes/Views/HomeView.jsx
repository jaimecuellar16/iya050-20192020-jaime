const { React, ReactDOM, Cards, Navbar, Sidebar } = window;

class HomeView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Sidebar />
        <Cards />
        <Navbar />
      </div>
    );
  }
}
