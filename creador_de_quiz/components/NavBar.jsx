const { React, ReactDOM } = window;
const { Link } = window.ReactRouterDOM;

class NavBar extends React.Component {
  render(props) {
    return (
      <nav className="navbar">
        <ul>
          <li>
            <Link to={"/categorias"}>Categorias</Link>
          </li>
          <li>
            <Link to={"/has-pregunta"}>Has tu propia pregunta</Link>
          </li>
        </ul>
      </nav>
    );
  }
}
