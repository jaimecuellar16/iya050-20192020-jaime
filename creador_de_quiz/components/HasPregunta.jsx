const { React, ReactDOM } = window;
class HasPregunta extends React.Component {
  constructor(props) {
    super(props);
    this.state = {tipo:" ",pregunta: " ", res1:"",res2:"",res3:"",res4:""};

    this.getSelectedValue = this.getSelectedValue.bind(this);
    this.getPregunta = this.getPregunta.bind(this);
    this.getRes1 = this.getRes1.bind(this);
    this.getRes2 = this.getRes2.bind(this);
    this.getRes3 = this.getRes3.bind(this);
    this.getRes4 = this.getRes4.bind(this);
    this.getRightAnswer = this.getRightAnswer.bind(this);
    this.enviarPregunta = this.enviarPregunta.bind(this);
  }
  getSelectedValue(event){
    this.setState({tipo:event.target.value});
  }
  getPregunta(event) {
    this.setState({ pregunta: event.target.value });
  }
  getRes1(event) {
    this.setState({ res1: event.target.value });
  }
  getRes2(event) {
    this.setState({ res2: event.target.value });
  }
  getRes3(event) {
    this.setState({ res3: event.target.value });
  }
  getRes4(event) {
    this.setState({ res4: event.target.value });
  }
  getRightAnswer(event) {}
  enviarPregunta() {
    console.log(this.state.pregunta);
  }
  render() {
    return (
      <form>
        <div className="categorias">
          <p>Llena los campos para poder agregar una pregunta</p>
          <select name="categorias">
            <option value="Ciencias">Ciencias</option>
            <option value="Historia">Historia</option>
            <option value="Deportes">Deportes</option>
            <option value="Geografia">Geografia</option>
          </select>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Pregunta"
            onChange={this.getPregunta}
          ></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 1"
            onChange={this.getRes1}
          ></input>
          <input type="checkbox" id="cbox1" value="first_checkbox"></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 2"
            onChange={this.getRes2}
          ></input>
          <input type="checkbox" id="cbox2" value="second_checkbox"></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 3"
            onChange={this.getRes3}
          ></input>
          <input type="checkbox" id="cbox3" value="third_checkbox"></input>
          <br />
          <input
            type="text"
            className="inp w3-input"
            placeholder="Respuesta 4s"
            onChange={this.getRes4}
          ></input>
          <input type="checkbox" id="cbox4" value="fourth_checkbox"></input>
          <br />
          <br />
          <a className="inp btn-answer" onClick={this.enviarPregunta}>
            Enviar pregunta
          </a>
        </div>
      </form>
    );
  }
}
