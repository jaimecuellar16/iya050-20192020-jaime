const { React, ReactDOM } = window;

class Preguntas extends React.Component {
  constructor(props) {
    super(props);
    let { match } = this.props;
    let id = match.params.id;
    var index = 0;
    switch (id) {
      case "Ciencias":
        index = 0;
        break;
      case "Historia":
        index = 1;
        break;
      case "Deportes":
        index = 2;
        break;
      case "Cultura General":
        index = 3;
        break;
    }
    this.state = { index: index, pregunta_index: 0 };
    console.log(id);
    this.responder = this.responder.bind(this);
    this.preguntas = [
      {
        id: "Ciencias",
        preguntas: [
          {
            pregunta: "¿Que teoria invento Alebert Einstein?",
            respuestas: [
              { res: "teoria relatividad", correcta: true },
              { res: "Teoria agujeros de gusanos", correcta: false },
              { res: "Teoria cuantica", correcta: false },
              { res: "Teoria x", correcta: false },
            ],
          },
          {
            pregunta: "¿Qué parte del cuerpo tiene 27 huesos y 35 músculos? ",
            respuestas: [
              { res: "Pie", correcta: false },
              { res: "Espalda", correcta: false },
              { res: "Craneo", correcta: false },
              { res: "Mano", correct: true },
            ],
          },
          {
            pregunta: "¿Qué órgano es el encargado de fabricar insulina?",
            respuestas: [
              { res: "Higado", correcta: false },
              { res: "Pancreas", correcta: true },
              { res: "Apendice", correcta: false },
              { res: "Corazon", correcta: false },
            ],
          },
        ],
      },
      {
        id: "Historia",
        preguntas: [
          {
            pregunta: "¿Qué país comenzó la Segunda Guerra Mundial?",
            respuestas: [
              { res: "Inglaterra", correcta: false },
              { res: "Alemania", correcta: true },
              { res: "EEUU", correcta: false },
              { res: "Francia", correcta: false },
            ],
          },
          {
            pregunta:
              "¿Quién fue el primer rey de Roma, según la tradición romana?",
            respuestas: [
              { res: "Romulo", correcta: true },
              { res: "Julio Cesar", correcta: false },
              { res: "Maximo", correcta: false },
              { res: "Claudio", correcta: false },
            ],
          },
          {
            pregunta: "¿En qué año sucedió el accidente de Chernóbil?",
            respuestas: [
              { res: "1970", correcta: false },
              { res: "1999", correcta: false },
              { res: "1980", correcta: false },
              { res: "1986", correcta: true },
            ],
          },
        ],
      },
      {
        id: "Deportes",
        preguntas: [
          {
            pregunta:
              "¿Qué duración tiene un partido de fútbol que llega a la tanda de penaltis?",
            respuestas: [
              { res: "90 Minutos", correcta: false },
              { res: "100 minutos", correcta: false },
              { res: "120 Minutos", correcta: true },
              { res: "115 Minutos", correcta: false },
            ],
          },
          {
            pregunta: "¿Quién tiene más balones de oro? ",
            respuestas: [
              { res: "Cristiano Ronaldo", correcta: false },
              { res: "Messi", correcta: true },
              { res: "Ronaldiho", correcta: false },
              { res: "Xavi", correcta: false },
            ],
          },
          {
            pregunta:
              "¿Qué equipo de baloncesto ha sido campeón de la NBA en 2012 y 2013?",
            respuestas: [
              { res: "Miami Head", correcta: true },
              { res: "LA Lakers", correcta: false },
              { res: "Golden State Warrios", correcta: false },
              { res: "Chicago Bulls", correcta: false },
            ],
          },
        ],
      },
      {
        id: "Geografia",
        preguntas: [
          {
            pregunta: "¿En qué país es típico el flamenco?",
            respuestas: [
              { res: "España", correcta: true },
              { res: "Argentina", correcta: false },
              { res: "Colombia", correcta: false },
              { res: "Francia", correcta: false },
            ],
          },
          {
            pregunta: "¿Cuántas estrellas tiene la bandera de China?",
            respuestas: [
              { res: "2", correcta: false },
              { res: "4", correcta: false },
              { res: "3", correcta: false },
              { res: "5", correcta: true },
            ],
          },
          {
            pregunta: "¿En qué país se encuentra Oslo?",
            respuestas: [
              { res: "Suiza", correcta: false },
              { res: "Noruega", correcta: true },
              { res: "Suecia", correcta: false },
              { res: "Dinamarca", correcta: false },
            ],
          },
        ],
      },
    ];
  }
  responder(event) {
    let respuesta = event.currentTarget.innerHTML;
    console.log(respuesta);
    var correcta = "";
    const index_pregunta = this.state.pregunta_index;
    const categoria = this.preguntas[this.state.index];
    const pregunta_actual = categoria.preguntas[this.state.pregunta_index];
    pregunta_actual.respuestas.map((elem) => {
      if (elem.correcta) {
        correcta = elem.res;
      }
      if (elem.res === respuesta) {
        if (elem.correcta) {
          alert("EXACTO, eres un genio, espera a la siguiente pregunta");
        } else {
          alert(
            `la respuesta correcta es: ${correcta}, Espera a la siguiente pregunta`
          );
        }
      }
    });
    console.log(pregunta_actual.respuestas.length);
    setTimeout(() => {
      if (index_pregunta + 1 >= pregunta_actual.respuestas.length - 1) {
        console.log("ERROR");
        this.setState({ pregunta_index: 0 });
      } else {
        console.log(index_pregunta);
        this.setState({ pregunta_index: this.state.pregunta_index + 1 });
      }
    }, 1000);
  }
  render() {
    const preguntas = [
      {
        id: "Ciencias",
        preguntas: [
          {
            pregunta: "¿Que teoria invento Alebert Einstein?",
            respuestas: [
              { res: "teoria relatividad", correcta: true },
              { res: "Teoria agujeros de gusanos", correcta: false },
              { res: "Teoria cuantica", correcta: false },
              { res: "Teoria x", correcta: false },
            ],
          },
          {
            pregunta: "¿Qué parte del cuerpo tiene 27 huesos y 35 músculos? ",
            respuestas: [
              { res: "Pie", correcta: false },
              { res: "Espalda", correcta: false },
              { res: "Craneo", correcta: false },
              { res: "Mano", correct: true },
            ],
          },
          {
            pregunta: "¿Qué órgano es el encargado de fabricar insulina?",
            respuestas: [
              { res: "Higado", correcta: false },
              { res: "Pancreas", correcta: true },
              { res: "Apendice", correcta: false },
              { res: "Corazon", correcta: false },
            ],
          },
        ],
      },
      {
        id: "Historia",
        preguntas: [
          {
            pregunta: "¿Qué país comenzó la Segunda Guerra Mundial?",
            respuestas: [
              { res: "Inglaterra", correcta: false },
              { res: "Alemania", correcta: true },
              { res: "EEUU", correcta: false },
              { res: "Francia", correcta: false },
            ],
          },
          {
            pregunta:
              "¿Quién fue el primer rey de Roma, según la tradición romana?",
            respuestas: [
              { res: "Romulo", correcta: true },
              { res: "Julio Cesar", correcta: false },
              { res: "Maximo", correcta: false },
              { res: "Claudio", correcta: false },
            ],
          },
          {
            pregunta: "¿En qué año sucedió el accidente de Chernóbil?",
            respuestas: [
              { res: "1970", correcta: false },
              { res: "1999", correcta: false },
              { res: "1980", correcta: false },
              { res: "1986", correcta: true },
            ],
          },
        ],
      },
      {
        id: "Deportes",
        preguntas: [
          {
            pregunta:
              "¿Qué duración tiene un partido de fútbol que llega a la tanda de penaltis?",
            respuestas: [
              { res: "90 Minutos", correcta: false },
              { res: "100 minutos", correcta: false },
              { res: "120 Minutos", correcta: true },
              { res: "115 Minutos", correcta: false },
            ],
          },
          {
            pregunta: "¿Quién tiene más balones de oro? ",
            respuestas: [
              { res: "Cristiano Ronaldo", correcta: false },
              { res: "Messi", correcta: true },
              { res: "Ronaldiho", correcta: false },
              { res: "Xavi", correcta: false },
            ],
          },
          {
            pregunta:
              "¿Qué equipo de baloncesto ha sido campeón de la NBA en 2012 y 2013?",
            respuestas: [
              { res: "Miami Head", correcta: true },
              { res: "LA Lakers", correcta: false },
              { res: "Golden State Warrios", correcta: false },
              { res: "Chicago Bulls", correcta: false },
            ],
          },
        ],
      },
      {
        id: "Geografia",
        preguntas: [
          {
            pregunta: "¿En qué país es típico el flamenco?",
            respuestas: [
              { res: "España", correcta: true },
              { res: "Argentina", correcta: false },
              { res: "Colombia", correcta: false },
              { res: "Francia", correcta: false },
            ],
          },
          {
            pregunta: "¿Cuántas estrellas tiene la bandera de China?",
            respuestas: [
              { res: "2", correcta: false },
              { res: "4", correcta: false },
              { res: "3", correcta: false },
              { res: "5", correcta: true },
            ],
          },
          {
            pregunta: "¿En qué país se encuentra Oslo?",
            respuestas: [
              { res: "Suiza", correcta: false },
              { res: "Noruega", correcta: true },
              { res: "Suecia", correcta: false },
              { res: "Dinamarca", correcta: false },
            ],
          },
        ],
      },
    ];
    var index = 0;
    return (
      <div className="categorias">
        <h2>
          {
            preguntas[this.state.index].preguntas[this.state.pregunta_index]
              .pregunta
          }
        </h2>
        <div>
          <div className="preguntas-container">
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[0].res
              }
            </a>
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[1].res
              }
            </a>
          </div>
          <div className="preguntas-container">
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[2].res
              }
            </a>
            <a className="btn-answer" onClick={this.responder}>
              {
                preguntas[this.state.index].preguntas[this.state.pregunta_index]
                  .respuestas[3].res
              }
            </a>
          </div>
        </div>
      </div>
    );
  }
}
