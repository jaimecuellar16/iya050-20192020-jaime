const { React, ReactDOM, Categorias, Preguntas, HasPregunta } = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/categorias">Categorias</Link>
              </li>
              <li>
                <Link to="/has-pregunta">Has tu pregunta</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route exact path="/" component={() => <Redirect to="/categorias" />} />
            <Route path="/creador_de_quiz" component={Categorias}></Route>
            <Route path="/preguntas/:id" component={Preguntas}></Route>
            <Route path="/categorias" component={Categorias}></Route>
            <Route path="/has-pregunta" component={HasPregunta}></Route>
          </Switch>
        </div>
      </Router>
    );
  }
}
