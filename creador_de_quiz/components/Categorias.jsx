const { React, ReactDOM } = window;
const { Link } = window.ReactRouterDOM;

class Categorias extends React.Component {
  render() {
    const categorias = [
      {
        id: 1,
        name: "Ciencias",
        icon:
          "https://cdn.icon-icons.com/icons2/539/PNG/512/beaker-atom_icon-icons.com_53031.png",
      },
      {
        id: 2,
        name: "Historia",
        icon: "https://alcaldianuevaguinea.com/img/bg-img/historia.png",
      },
      {
        id: 3,
        name: "Deportes",
        icon: "https://www.cuperacredita.es/img/iconos/icono-deportes.svg",
      },
      {
        id: 4,
        name: "Geografia",
        icon: "https://image.flaticon.com/icons/png/512/344/344403.png",
      },
    ];
    return categorias.map((elem) => (
      <div className="categorias" key={elem.id}>
        <img src={elem.icon}></img>
        <br />
        <Link to={`/preguntas/${elem.name}`}>
          <p>{elem.name}</p>
        </Link>
      </div>
    ));
  }
}
